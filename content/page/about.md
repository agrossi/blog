---
title: About me
subtitle: Hello there! My name is Andrew Grossi.
comments: false
---

I live in [Chicago](https://en.wikipedia.org/wiki/Chicago) and I am an [Applications Development Manager](https://www.infoq.com/articles/development-manager-role) at [CNA](https://www.cna.com).  I also enjoy building things on the side which explains why I have this site rather than just a [LinkedIn](https://www.linkedin.com/in/andrewgrossi) page.
