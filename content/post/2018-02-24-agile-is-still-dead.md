---
title:	Agile is still dead
date: 2018-02-24
last_update: 2018-02-24
tags: []
categories:
---

Such a humorous and informational presentation from 2015 which still holds true today on essentially how the basis for what is considered "agile software development" has been bastardized into a sales buzz word.

<div style="position: relative; padding-bottom: 56.25%; padding-top: 30px; height: 0; overflow: hidden;">
  <iframe src="https://www.youtube.com/embed/a-BOSpxYJ9M"
  style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" allowfullscreen frameborder="0" title="YouTube Video"></iframe>
</div>

His short blog post read: 

https://pragdave.me/blog/2014/03/04/time-to-kill-agile.html

The "Manifesto for Agile Software Development" not "Agile Manifesto":

http://agilemanifesto.org/