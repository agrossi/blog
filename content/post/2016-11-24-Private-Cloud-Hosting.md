---
title:  Private cloud hosting
date: 2016-11-24
last_update: 2016-11-24
tags: []
categories:
---

TLDR; Configuration of https://www.seafile.net as a Dropbox and iCloud alternative.

I recently cleaned up some of my personal data and did some research on the various approaches to make this data available for me across my devices.  I initially leaned toward the big proprietary services like iCloud or Dropbox, but was concerned about putting all my information in the cloud and in the hands of these corporations.  I did some research and identified a couple of options which would offer more control of private data, but still utilize the benefits of the cloud.  I would recommend looking at the following solutions to fit any similar needs that you may have.


1. Nextcloud - Open Source iCloud or Dropbox replacement. Contains a full suite of services like file sync, contacts, calendar, etc...

https://nextcloud.com

2. Seafile - file-hosting software system.  Does not contain all the  bells and whistles that Nextcloud offers.  As of now, this is the most recommended solution for file-sync only as it is the faster than the others.

https://www.seafile.net

3. Veracrypt - his allows files to be encrypted locally and utilizes the cloud platform as "dumb" storage.  This offers the benefit of using commercial cloud services like Dropbox, but with the added benefit of security by protecting the entire contents of the file system.

https://veracrypt.codeplex.com


Cheers,

Andrew
