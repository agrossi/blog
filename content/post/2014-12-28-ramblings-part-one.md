---
title:  Ramblings - Part I
date: 2014-12-28
last_update:  2014-12-28
tags: []
categories:
---
** The Pitch - **

The pitch is one of the most important components in communicating to the outside world. The quantity and speed at which information travels requires you to be more focused on differentiating and defining yourself in a larger context. This applies to everywhere you come in contact with external parties: your company, business, relationships...

** Fun - **

I've undervalued the concept of fun because many topics I've read or learned relating to the topic of success focus on principles such as hard work, focus, and determination. While these elements are important, I've learned the underlying foundation of success must be based on something you actually enjoy.
