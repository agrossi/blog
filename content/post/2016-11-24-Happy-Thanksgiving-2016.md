---
title:  Happy Thanksgiving - 2016!
date: 2016-11-24
last_update: 2016-11-24
tags: []
categories:
---

Happy Thanksgiving!

It's hard to believe my last post here was about 2 years ago.  Time sure flies.  I'll be working on a personal project on managing personal data in the cloud by using local encryption as well as self-managed cloud solutions to eliminate relying on proprietary platforms like iCloud or Dropbox.


Cheers,

Andrew
