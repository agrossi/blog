---
title: Development bucket list 2014
date: 2014-12-28
last_update: 2014-12-28
tags: []
categories:
---

* Use Git (Done)
* Create a static blog using Jekyll (Done)
* Learn VIM (In Progress)
* Build an iOS App
