---
title: First post using Hugo
date: 2018-02-10
last_update: 2018-02-10
tags: []
categories:
---

Hello World!

I have heard good things about the [Hugo](https://gohugo.io/) static site generator for hosting a personal blog so I thought I would give it a try.  I also have created a [Jekyll](https://jekyllrb.com/) site on [GitHub](http://andrewgrossi.github.io/) in the past, but it's 2018 so I thought about trying something new (although I don't really post much).

Sincerely,

Andrew
